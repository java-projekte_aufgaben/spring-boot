package com.dailycodebuffer.Springboot.tutorial.controller;

import com.dailycodebuffer.Springboot.tutorial.entity.Employee;
import com.dailycodebuffer.Springboot.tutorial.service.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

        @Autowired
        private MockMvc mockMvc;

        /**
         * @MockBean-Annotation die verwendet werden kann, um Mocks zu einem Spring ApplicationContext hinzuzufügen.
         */
        @MockBean
        private EmployeeService employeeService;

        private Employee employee;

        /**
         * @BeforeEach wird verwendet, um zu signalisieren, dass die annotierte Methode vor jedem @Test ausgeführt werden soll.
         */
        @BeforeEach
        void setUp() {
            employee = Employee.builder()
                    .employeeAddress("Landeck")
                    .firstName("Michael")
                    .lastName("Beer")
                    .employeeId(1L)
                    .build();
        }

        /**
         * Methode um das Speichern eines Employees zu testen.
         * @throws Exception
         */
        @Test
        void saveEmployee() throws Exception {
            Employee inputEmployee = Employee.builder()
                    .employeeAddress("Landeck")
                    .firstName("Michael")
                    .lastName(("Beer"))
                    .build();

            Mockito.when(employeeService.saveEmployee(inputEmployee)).thenReturn(employee);

            mockMvc.perform(post("/employees")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\n" +
                                    "\t\"employeeAddress\":\"Landeck\",\n" +
                                    "\t\"employeeFirstName\":\"Michael\",\n" +
                                    "\t\"employeeLastName\":\"Beer\",\n" +
                                    "}"))
                    .andExpect(status().isOk());

        }

        @Test
        void fetchEmployeeById() throws Exception {
            Mockito.when(employeeService.fetchEmployeeById(1L))
                    .thenReturn(employee);

            mockMvc.perform(get("/employees/1")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.lastName")
                            .value(employee.getLastName()));
        }
}

