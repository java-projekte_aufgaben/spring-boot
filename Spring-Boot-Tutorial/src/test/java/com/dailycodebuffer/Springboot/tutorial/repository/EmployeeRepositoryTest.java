package com.dailycodebuffer.Springboot.tutorial.repository;

import com.dailycodebuffer.Springboot.tutorial.entity.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Bietet eine Untermenge von EntityManager-Methoden, die für Tests nützlich sind, sowie Hilfsmethoden für allgemeine Testaufgaben.
     */
    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {
        Employee employee =
                Employee.builder()
                        .firstName("Michael")
                        .lastName("Beer")
                        .employeeAddress("Landeck")
                        .build();

        entityManager.persist(employee);
    }

    @Test
    public void whenFindById_thenReturnDepartment(){
        Employee employee = employeeRepository.findById(1L).get();
        assertEquals(employee.getFirstName(),"Michael");
    }
}
