package com.dailycodebuffer.Springboot.tutorial.service;

import com.dailycodebuffer.Springboot.tutorial.entity.Employee;
import com.dailycodebuffer.Springboot.tutorial.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @MockBean
    private EmployeeRepository employeeRepository;

  /*  @BeforeEach
    void setUp(){
        Employee employee =
                Employee.builder()
                        .firstName("Michael")
                        .lastName("Beer")
                        .employeeAddress("Landeck")
                        .employeeId(1L)
                        .build();

        Mockito.when(employeeRepository.findByEmployeeByLastNameIgnoreCase("Beer")).thenReturn(employee);
    }

    @Test
    @DisplayName("Get Data based on Valida employee LastName")
    public void whenValidEmployeeLastName_thenEmployeeShouldFound(){
        String lastName = "Beer";
        Employee found = employeeService.fetchEmployeeByLastName(lastName);

        assertEquals(lastName, found.getLastName());
    }*/
}
