package com.dailycodebuffer.Springboot.tutorial.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${welcome.message}")
    private String welcomeMessage;

    /**
     * Methode ist unter der URL http://localhost:8082/ erreichbar.
     * Als Parameter kann ein String übergben werden, indem spezielle
     * URL Angaben wie Path Variablen definiert werden können.
     * @return welcomeMessage
     */
    @GetMapping("/")
    public String helloWorld(){
     return welcomeMessage;
    }
}
