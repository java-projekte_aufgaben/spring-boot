package com.dailycodebuffer.Springboot.tutorial.error;

public class EmployeeNotFoundException extends Exception{

    public EmployeeNotFoundException(String message) {

        super(message);
    }
}
