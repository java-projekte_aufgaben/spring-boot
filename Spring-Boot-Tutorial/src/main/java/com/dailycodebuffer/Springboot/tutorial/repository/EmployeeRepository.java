package com.dailycodebuffer.Springboot.tutorial.repository;

import com.dailycodebuffer.Springboot.tutorial.entity.Department;
import com.dailycodebuffer.Springboot.tutorial.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository für Employees.
 * @Repository wird verwendet um anzugeben, dass die Klasse den Mechanismus zum
 * Speichern, Abrufen, Suchen, Aktualisieren und Löschen von Objekten bereitstellt.
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

  //  Employee findByEmployeeByLastNameIgnoreCase(String lastName);

}
