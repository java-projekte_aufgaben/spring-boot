package com.dailycodebuffer.Springboot.tutorial.controller;

import com.dailycodebuffer.Springboot.tutorial.entity.Department;
import com.dailycodebuffer.Springboot.tutorial.error.DepartmentNotFoundException;
import com.dailycodebuffer.Springboot.tutorial.service.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @RestController ist eine spezielle Version des Controllers. Es enthält die
 * Annotationen @Controller und @ResponseBody und vereinfacht dadurch die Controller-Implementierung.
 * Jede Anforderungsbehandlungsmethode der Controller-Klasse serialisiert automatisch
 * Rückgabeobjekte in HttpResponse.
 */
@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    /**
     * Die Schnittstelle org.slf4j.Logger ist der Haupteinstiegspunkt für den Benutzer in die SLF4J-API.
     * Es wird erwartet, dass die Protokollierung durch konkrete Implementierungen dieser Schnittstelle erfolgt.
     */
    private final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);

    /**
     * Speichert ein neues Department.
     * Logger.info protokolliert eine Nachricht auf der Eben INFO gemäß dem angegebenen Format und Argument.
     * @PostMapping Annotation für einen HTTP POST request.
     * @param department
     * @return
     */
    @PostMapping("/departments")
    public Department saveDepartment(@Valid @RequestBody Department department){
        LOGGER.info("Inside saveDepartment of DepartmentController");
        return departmentService.saveDepartment(department);
    }

    /**
     * Liste mit allen Departments
     * @GetMapping Annotation für einen HTTP GET request.
     * @return Liste aller Departments
     */
    @GetMapping("/departments")
    public List<Department> fetchDepartmentList(){
        LOGGER.info("Inside fetchDepartmentList of DepartmentController");
        return departmentService.fetchDepartmentList();
    }

    /**
     * Department per ID
     * @param departmentId
     * @return
     * @throws DepartmentNotFoundException
     */
    @GetMapping("/departments/{id}")
    public Department fetchDepartmentById(@PathVariable("id") Long departmentId) throws DepartmentNotFoundException {
        return departmentService.fetchDepartmentById(departmentId);
    }

    /**
     * Department per ID löschen.
     * @DeleteMapping Annotation für einen HTTP Delete request. ID wird über HTTP-Pfad ermittelt.
     * @param departmentId
     * @return message
     */
    @DeleteMapping("/departments/{id}")
    public String deleteDepartmentById(@PathVariable("id") Long departmentId){
        departmentService.deleteDepartmentById(departmentId);
        return "Department deleted successfully!!";
    }

    /**
     * Department updaten
     * @PutMapping Annotation für einen HTTP PUT request. ID wird über HTTP-Pfad ermittelt.
     * @param departmentId
     * @param department
     * @return upgedatetes Department
     */
    @PutMapping("/departments/{id}")
    public Department updateDepartment(@PathVariable("id") Long departmentId,
                                       @RequestBody Department department){
        return departmentService.updateDepartment(departmentId,department);
    }

    /**
     * Department per Name
     * @param departmentName
     * @return Department per Name
     */
    @GetMapping("/departments/name/{name}")
    public Department fetchDepartmentByName(@PathVariable("name") String departmentName){
        return departmentService.fetchDepartmentByName(departmentName);
    }
}
