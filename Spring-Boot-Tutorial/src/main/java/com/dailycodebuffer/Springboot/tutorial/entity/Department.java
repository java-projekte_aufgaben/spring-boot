package com.dailycodebuffer.Springboot.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Für jede Entität müssen mindestens zwei Annotationen definiert sein: @Entity und @Id.
 * @Entity-Annotation gibt an, dass die Klasse eine Entität ist und auf eine Datenbanktabelle abgebildet wird.
 * @Data-Annotation Erzeugt Getter für alle Felder, eine nützliche toSting-Methode sowie HAshCode- und Equals-Implementierungen,
 * die alle nicht transienten Felder überprüften. Erzeugt auch Setter für alle nicht-finalen Felder sowie einen Konstruktor.
 * @Id-Annotation spezifiziert den Primärschlüssel einer Entität.
 * @NoArgsConstructor Constructor ohne Parameter.
 * @AllArgsConstructor Constructor mit allen Parametern.
 * @Builder erzeugt komplexe Builder-APIs
 * @GeneratedValue-Annotation ermöglicht die Angabe von Generierungsstrategien für die Werte von Primärschlüsseln.
 * @NotBlank Überprüft, ob der Wert weder null noch leer ist.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long departmentId;

    @NotBlank(message = "Please Add Department Name")
    private String departmentName;
    private String departmentAddress;
    private String departmentCode;


    /**
     * Referenzierung zur Entität Employee
     */
    @OneToMany(mappedBy = "department")
    private List<Employee> employeeList;

}
