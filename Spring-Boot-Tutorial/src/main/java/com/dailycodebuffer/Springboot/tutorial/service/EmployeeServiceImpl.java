package com.dailycodebuffer.Springboot.tutorial.service;

import com.dailycodebuffer.Springboot.tutorial.entity.Employee;
import com.dailycodebuffer.Springboot.tutorial.error.EmployeeNotFoundException;
import com.dailycodebuffer.Springboot.tutorial.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> fetchEmployeeList() {
        return employeeRepository.findAll();
    }


    @Override
    public Employee fetchEmployeeById(Long employeeId) throws EmployeeNotFoundException {
        Optional<Employee> employee = employeeRepository.findById(employeeId);

        if (!employee.isPresent()) {
            throw new EmployeeNotFoundException("Employee not available");
        } else {
            return employee.get();
        }
   }

    @Override
    public void deleteEmployeeById(Long employeeId) {

        employeeRepository.deleteById(employeeId);
    }

    @Override
    public Employee updateEmployee(Long employeeId, Employee employee) throws EmployeeNotFoundException {

        Optional<Employee> empDB = employeeRepository.findById(employeeId);
        if (!empDB.isPresent()) {
            throw new EmployeeNotFoundException("Employee Not Available!");
        }
        if (Objects.nonNull(employee.getFirstName()) && !"".equalsIgnoreCase(employee.getFirstName())) {
            empDB.get().setFirstName(employee.getFirstName());
        }

        if (Objects.nonNull(employee.getLastName()) && !"".equalsIgnoreCase(employee.getLastName())) {
            empDB.get().setLastName(employee.getLastName());
        }

        if (Objects.nonNull(employee.getEmployeeAddress()) && !"".equalsIgnoreCase(employee.getEmployeeAddress())) {
            empDB.get().setEmployeeAddress(employee.getEmployeeAddress());
        }
        return employeeRepository.save(empDB.get());
    }

  /*  @Override
    public Employee fetchEmployeeByLastName(String lastName) {
        return employeeRepository.findByEmployeeByLastNameIgnoreCase(lastName);
    }*/

}
