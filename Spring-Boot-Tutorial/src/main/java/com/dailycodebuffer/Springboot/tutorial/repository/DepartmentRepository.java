package com.dailycodebuffer.Springboot.tutorial.repository;

import com.dailycodebuffer.Springboot.tutorial.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository für Departments.
 * @Repository wird verwendet, um anzugeben, dass die Klasse den Mechanismus zum
 * Speichern, Abrufen, Suchen, Aktualisieren und Löschen von Objekten bereitstellt.
 */
@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    Department findByDepartmentName(String departmentName);

    Department findByDepartmentNameIgnoreCase(String departmentName);
}
