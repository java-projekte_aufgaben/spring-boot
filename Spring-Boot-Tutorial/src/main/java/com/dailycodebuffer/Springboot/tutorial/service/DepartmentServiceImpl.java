package com.dailycodebuffer.Springboot.tutorial.service;

import com.dailycodebuffer.Springboot.tutorial.entity.Department;
import com.dailycodebuffer.Springboot.tutorial.error.DepartmentNotFoundException;
import com.dailycodebuffer.Springboot.tutorial.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @Service gibt an, dass diese Klasse die Business-Logik beinhaltet.
 */
@Service
public class DepartmentServiceImpl implements DepartmentService{

    /**
     * @Autowired um auf die Methoden des DepartmentRepository zugreifen zu können.
     */
    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * Methode zum speichern eines Departments.
     * @param department
     * @return Speichert das Department über Methode von departmentRepository.
     */
    @Override
    public Department saveDepartment(Department department) {
        return departmentRepository.save(department);
    }

    /**
     * Methode um eine Liste des Departments zu bekommen.
     * @return Alle Departments
     */
    @Override
    public List<Department> fetchDepartmentList() {
        return departmentRepository.findAll();
    }

    /**
     * Methode um Department per Id zu bekommen.
     * @param departmentId
     * @return Department nach Id.
     * @throws DepartmentNotFoundException
     */
    @Override
    public Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException {
        Optional<Department> department = departmentRepository.findById(departmentId);

        if(!department.isPresent()){
            throw new DepartmentNotFoundException("Department Not Available");
        }
        return department.get();
    }

    /**
     * Department nach Id löschen.
     * @param departmentId
     */
    @Override
    public void deleteDepartmentById(Long departmentId) {
        departmentRepository.deleteById(departmentId);
    }

    /**
     * Methode um ein Department zu updaten.
     * @param departmentId
     * @param department
     * @return Speichert das bearbeitete Department.
     */
    @Override
    public Department updateDepartment(Long departmentId, Department department) {
        Department depDB = departmentRepository.findById(departmentId).get();

        if(Objects.nonNull(department.getDepartmentName()) && !"".equalsIgnoreCase(department.getDepartmentName())){
            depDB.setDepartmentName(department.getDepartmentName());
        }

        if(Objects.nonNull(department.getDepartmentCode()) && !"".equalsIgnoreCase(department.getDepartmentCode())){
            depDB.setDepartmentCode(department.getDepartmentCode());
        }

        if(Objects.nonNull(department.getDepartmentAddress()) && !"".equalsIgnoreCase(department.getDepartmentAddress())){
            depDB.setDepartmentAddress(department.getDepartmentAddress());
        }

        return departmentRepository.save(depDB);
    }

    /**
     * Methode um das Department per Name zu bekommen.
     * @param departmentName
     * @return Department Namen.
     */
    @Override
    public Department fetchDepartmentByName(String departmentName) {
        return departmentRepository.findByDepartmentNameIgnoreCase(departmentName);
    }
}
